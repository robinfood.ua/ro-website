const path = require('path');

module.exports = {
  pluginOptions: {
    'style-resources-loader': {
      preProcessor: 'scss',
      patterns: [
        '/Users/katia/Desktop/app/my-app/src/styles/variables.scss'
      ]
    },
    i18n: {
      locale: 'en',
      fallbackLocale: 'ua',
      localeDir: 'locales',
      enableInSFC: false,
      enableBridge: false
    }
  }
};
