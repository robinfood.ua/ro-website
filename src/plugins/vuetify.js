import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';
import en from '../locales/en'
import ua from '../locales/ua'

Vue.use(Vuetify);

export default new Vuetify({
    lang: {
        locales: { en, ua },
        current: 'ua',
    },
});
